/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.tecnm.morelia.tap_1up3.models;

import java.io.Serializable;

/**
 *
 * @author Wallace
 */
public class Alumno implements Serializable {
    private String numControl;

    private String nombre;
    private String carrera;
    private int semestre;
    
    public Alumno(){}
    
    public Alumno(String numControl, String nombre, String carrera, int semestre) {
        this.numControl = numControl;
        this.nombre = nombre;
        this.carrera = carrera;
        this.semestre = semestre;
    }

    public String getNumControl() {
        return numControl;
    }

    public void setNumControl(String numControl) {
        this.numControl = numControl;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public int getSemestre() {
        return semestre;
    }

    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }

    /*public String imprime() {
        return "Alumno{" + "numControl=" + numControl + ", nombre=" + nombre + ", carrera=" + carrera + ", semestre=" + semestre + '}';
    }
*/

}
