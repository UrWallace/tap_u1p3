/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.tecnm.morelia.tap_1up3.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import mx.tecnm.morelia.tap_1up3.models.Alumno;

/**
 *
 * @author Wallace
 */
public class Tools {
    public static ArrayList<Alumno> leerListaAlumnos(File archivo) {
        ArrayList<Alumno> alumnos = new ArrayList<>();
        try {
            FileInputStream fis = new FileInputStream(archivo);
            ObjectInputStream ois = new ObjectInputStream(fis);
            alumnos =(ArrayList<Alumno>)ois.readObject();
            ois.close();
            
            //
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Muestra alumnos en consola
        for(Alumno a : alumnos){
            System.out.println(a.getNumControl());
            System.out.println(a.getNombre());
            System.out.println(a.getCarrera());
            System.out.println(""+a.getSemestre());
        }
    return alumnos;
    }
}
